﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.MeterExportAPI.Models;

namespace TPDDL.MeterExportAPI.Helper
{
    public class Equipment
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;

        public int GetEquipmentIdByUtilityId(string utilityId)
        {
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select ID from Equipment where Utility_ID='" + utilityId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["ID"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetEquipmentIdByParam(string param, string value)
        {
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    string result = "";
                    connection.Open();
                    string query = "select Meter_ID from Equipment where " + param + "='" + value + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = reader["Meter_ID"].ToString();
                    }
                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckEquipmentIdById(int id)
        {
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    bool result = false;
                    int count = 0;
                    connection.Open();
                    string query = "select 1 from Equipment where ID='" + id + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        count = Convert.ToInt32(reader["count"].ToString());
                    }
                    if (count == 1)
                    {
                        result = true;
                    }
                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public EquipmentModel GetEquipmentByUtilityId(string utilityId)
        {
            try
            {
                EquipmentModel equipmentModel = new EquipmentModel();

                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from Equipment where Utility_ID='" + utilityId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        equipmentModel.ID = Convert.ToInt32(reader["ID"].ToString());
                        equipmentModel.MeterID = reader["Meter_ID"].ToString();
                    }

                    connection.Close();
                    return equipmentModel;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public EquipmentModel GetEquipmentById(string meterID)
        {
            try
            {
                EquipmentModel equipmentModel = new EquipmentModel();

                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from Equipment where Meter_ID='" + meterID + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        equipmentModel.ID = Convert.ToInt32(reader["ID"].ToString());
                        equipmentModel.MeterID = reader["Meter_ID"].ToString();
                    }
                    connection.Close();
                    return equipmentModel;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}