﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace TPDDL.MeterExportAPI.Helper
{
    public class SourceMaster
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public string GetSourceMasterIDByName(string name)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    string result = "";
                    connection.Open();
                    string query = "select ID from Source_Master where Name='" + name + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = reader["ID"].ToString();
                    }
                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}