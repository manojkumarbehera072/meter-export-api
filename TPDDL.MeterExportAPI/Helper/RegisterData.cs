﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.MeterExportAPI.Models;

namespace TPDDL.MeterExportAPI.Helper
{
    public class RegisterData
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public List<RegisterDataModel> GetRegisterDataByParam(int paramID, DateTime starDateTime, DateTime endDateTime)
        {
            List<RegisterDataModel> registerDataList = new List<RegisterDataModel>();
            try
            {
                string fields = "";
                if (starDateTime != null && starDateTime!=DateTime.MinValue && endDateTime!=null && endDateTime!=DateTime.MinValue)
                {
                    fields = "and (END_TIME between '" + starDateTime.ToString("yyyy-MM-dd") + "' and '" + endDateTime.ToString("yyyy-MM-dd") + "')";
                }

                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from REGISTER_DATA where PARAM_ID='" + paramID + "'" + fields;
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        RegisterDataModel registerData = new RegisterDataModel();
                        registerData.EquipmentId = reader["EQUIPMENT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EQUIPMENT_ID"].ToString());
                        registerData.DeviceCode = reader["DEVICE_CODE"] == DBNull.Value ? "null" : reader["DEVICE_CODE"].ToString();
                        registerData.ParamId = reader["PARAM_ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["PARAM_ID"].ToString());
                        registerData.EndTime = reader["END_TIME"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["END_TIME"].ToString());
                        registerData.ParamValue = reader["PARAM_VALUE"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["PARAM_VALUE"].ToString());
                        registerData.DemandPeakTiime = reader["DEMAND_PEAK_TIME"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["DEMAND_PEAK_TIME"].ToString());
                        registerData.InsertedBy = reader["INSERTED_BY"] == DBNull.Value ? "null" : reader["INSERTED_BY"].ToString();
                        registerData.InsertedDate = reader["INSERTED_DATE"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["INSERTED_DATE"].ToString());
                        registerData.Status = reader["STATUS"] == DBNull.Value ? "null" : reader["STATUS"].ToString();
                        registerData.Flag = reader["FLAG"] == DBNull.Value ? 0 : Convert.ToInt32(reader["FLAG"].ToString());
                        registerData.DataSource = reader["DATA_SOURCE"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DATA_SOURCE"].ToString());
                        registerData.UpdatedBy = reader["UPDATED_BY"] == DBNull.Value ? "null" : reader["UPDATED_BY"].ToString();
                        registerData.UpdatedOn = reader["UPDATED_ON"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["UPDATED_ON"].ToString());
                        registerDataList.Add(registerData);
                    }
                    connection.Close();
                    return registerDataList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}