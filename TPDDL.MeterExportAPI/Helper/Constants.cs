﻿namespace TPDDL.MeterExportAPI.Helper
{
    public class Constants
    {
        public const string Equipment = "Equipment";
        public const string Consumer = "Consumer";
        public const string Dev_Loc = "DeviceLocation";

        public const string EquipmentTable = "Equipment";
        public const string ConsumerTable = "Consumer";
        public const string DevLocTable = "Device_Location";

        public const string EquipmentAttrTable = "Equipment_Attrib";
        public const string ConsumerAttrTable = "Consumer_Attrib";
        public const string DevLocAttrTable = "Device_Location_Attrib";
    }
}