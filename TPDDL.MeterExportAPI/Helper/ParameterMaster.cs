﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.MeterExportAPI.Models;

namespace TPDDL.MeterExportAPI.Helper
{
    public class ParameterMaster
    {

        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public List<ParameterMasterModel> GetIdByDataTypeTag(string UoMCategory, string UoMSubCategory, string ParamName, string ParamType)
        {
            string fields = "";
            if (!String.IsNullOrEmpty(UoMCategory))
            {
                fields += " UOM_CATEGORY =" + "'" + UoMCategory + "' and";
            }
            if (!String.IsNullOrEmpty(UoMSubCategory))
            {
                fields += " UOM_SubCategory =" + "'" + UoMSubCategory + "' and";
            }
            if (!String.IsNullOrEmpty(ParamName))
            {
                fields += " PARAM_NAME =" + "'" + ParamName + "' and";
            }
            if (!String.IsNullOrEmpty(ParamType))
            {
                fields += " PARAM_TYPE =" + "'" + ParamType + "' and";
            }
            fields = fields.TrimEnd('a', 'n', 'd');
            List<ParameterMasterModel> parameterMasterModelList = new List<ParameterMasterModel>();
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select ID,Param_Type from Parameter_Master where" + fields + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ParameterMasterModel parameterMasterModel = new ParameterMasterModel();
                        parameterMasterModel.ID = reader["ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ID"].ToString());
                        parameterMasterModel.ParamType = reader["Param_Type"].ToString();
                        parameterMasterModelList.Add(parameterMasterModel);
                    }
                    connection.Close();
                    return parameterMasterModelList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}