﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using TPDDL.MeterExportAPI.Helper;
using TPDDL.MeterExportAPI.Models;
using Equipment = TPDDL.MeterExportAPI.Helper.Equipment;
using Equipments = TPDDL.MeterExportAPI.Models.Equipment;

namespace TPDDL.MeterExportAPI
{
    [WebService(Namespace = "http://tatapower-uds.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class UDSGetMeterDataAPI : System.Web.Services.WebService
    {
        
        [WebMethod]
        public MeterDataResponse MeterData_RequestIn(MeterDataRequest meterDataRequest)
        {
            string meterID = "";
            string responseCode = "";
            string responseText = "";
            string paramName = "";
            EquipmentModel equipmentModel = new EquipmentModel();
            Equipment equipmentObj = new Equipment();
            ParameterMaster parameterMaster = new ParameterMaster();
            List<ParameterMasterModel> parameterMasterModelList = new List<ParameterMasterModel>();
            MeterDataResponse response = new MeterDataResponse();
            SourceMaster sourceMaster = new SourceMaster();

            if (!String.IsNullOrEmpty(meterDataRequest.Source))
            {
                string _id = sourceMaster.GetSourceMasterIDByName(meterDataRequest.Source);
                if (String.IsNullOrEmpty(_id))
                {
                    responseCode = "1";
                    responseText = "Invalid Source";
                }
            }

            if (!String.IsNullOrEmpty(meterDataRequest.ParamName))
            {
                paramName = meterDataRequest.ParamName;
            }

            if (!String.IsNullOrEmpty(meterDataRequest.ObjectIdentifier))
            {
                if (meterDataRequest.ObjectIdentifier == Constants.Equipment)
                {
                    if (meterDataRequest.ObjectIdentifierType == "UtilityID")
                    {
                        meterID = equipmentObj.GetEquipmentIdByParam("Utility_ID", meterDataRequest.ObjectId);
                    }
                    if (meterDataRequest.ObjectIdentifierType == "MeterID")
                    {
                        meterID = equipmentObj.GetEquipmentIdByParam("Meter_ID", meterDataRequest.ObjectId);
                    }
                    if (meterDataRequest.ObjectIdentifierType == "AltUtilityID")
                    {
                        meterID = equipmentObj.GetEquipmentIdByParam("Alt_Utility_ID", meterDataRequest.ObjectId);
                    }
                    if (meterDataRequest.ObjectIdentifierType == "ID")
                    {
                        meterID = equipmentObj.GetEquipmentIdByParam("ID", meterDataRequest.ObjectId);
                    }
                }
                else if (meterDataRequest.ObjectIdentifier == Constants.Dev_Loc)
                {
                    //do stuff
                }
                else if (meterDataRequest.ObjectIdentifier == Constants.Consumer)
                {
                    //do stuff
                }
                else
                {
                    responseCode = "1";
                    responseText = "Invalid MasterObject ID";
                }
            }

            if (meterDataRequest.StarDateTime < meterDataRequest.EndDateTime && meterDataRequest.EndDateTime < DateTime.Now)
            {
                //do stuff
            }
            else
            {
                responseCode = "1";
                responseText = "Invalid Time in Request";
            }

            if (!String.IsNullOrEmpty(meterDataRequest.DataType))
            {
                if (meterDataRequest.DataType == "Read")
                {
                    if (!String.IsNullOrEmpty(meterDataRequest.UoMCategory) || !String.IsNullOrEmpty(meterDataRequest.UoMSubCategory) || !String.IsNullOrEmpty(meterDataRequest.ParamName)
                        || !String.IsNullOrEmpty(meterDataRequest.ParamType))
                    {
                        parameterMasterModelList = parameterMaster.GetIdByDataTypeTag(meterDataRequest.UoMCategory, meterDataRequest.UoMSubCategory, meterDataRequest.ParamName, meterDataRequest.ParamType);
                        if (parameterMasterModelList == null)
                        {
                            responseCode = "1";
                            responseText = "UoM or ParamName is invalid";
                        }
                        if (parameterMasterModelList[0].ID == 0)
                        {
                            responseCode = "1";
                            responseText = "Invalid ParamID";
                        }
                        //if (equipmentID == parameterMasterModel.ID)
                        //{

                        //}
                    }
                    else
                    {
                        responseCode = "1";
                        responseText = "Mandatory input tag missing";
                    }
                }
                else if (meterDataRequest.DataType == "Event")
                {
                    //do stuff
                }
                else
                {
                    responseCode = "1";
                    responseText = "Invalid DataType";
                }
            }
            List<Equipments> equipmentsList = new List<Equipments>();
            response.Equipment = equipmentsList;
            foreach (var parameterMasterModel in parameterMasterModelList)
            {
                Equipments equipments = new Equipments();
                RegisterData registerData = new RegisterData();
                List<RegisterDataModel> registerDataList = new List<RegisterDataModel>();
                IntervalData intervalData = new IntervalData();
                List<IntervalDataModel> intervalDataList = new List<IntervalDataModel>();
                if (parameterMasterModel.ParamType == "Register")
                {
                    if (meterDataRequest.EstValues == "Y")
                    {
                        registerDataList = registerData.GetRegisterDataByParam(parameterMasterModel.ID, meterDataRequest.StarDateTime, meterDataRequest.EndDateTime);

                        if (registerDataList.Count == 0)
                        {
                            responseCode = "1";
                            responseText = "No Data Available";
                        }
                        if (!String.IsNullOrEmpty(meterDataRequest.EstValues))
                        {
                            if (meterDataRequest.EstValues == "N")
                                registerDataList = registerDataList.Where(x => x.Status != "EST").ToList();
                        }

                        List<MeterRead> meterReadList = new List<MeterRead>();
                        List<EventData> eventDataList = new List<EventData>();
                        EventData eventData = new EventData();
                        EventAttrib eventAttrib = new EventAttrib();
                        equipments.MeterRead = meterReadList;
                        equipmentsList.Add(equipments);
                        foreach (var registerDatas in registerDataList)
                        {
                            MeterRead meterRead = new MeterRead();
                            response.ObjectIdentifier = meterDataRequest.ObjectIdentifier;
                            response.ObjectId = meterDataRequest.ObjectId;
                            equipmentModel = equipmentObj.GetEquipmentById(meterID);
                            equipments.EquipmentID = equipmentModel.MeterID;
                            equipments.MeterReadCount = registerDataList.Count;
                            meterRead.ParamName = paramName;
                            meterRead.ParamValue = registerDatas.ParamValue;
                            meterRead.DateTime = registerDatas.EndTime;
                            meterRead.Status = registerDatas.Status;
                            meterRead.Flag = registerDatas.Flag;
                            meterRead.DataSource = registerDatas.DataSource.ToString();
                            meterReadList.Add(meterRead);

                            //Assign value is not declared in SRS document.
                            //eventData.EventHeader.EventID= 
                            //eventData.EventHeader.EventDesc= 
                            //eventData.EventHeader.EventDateTime= 
                            //eventData.EventHeader.ReportingDateTime= 
                            //eventData.EventHeader.Source=
                            eventDataList.Add(eventData);
                            //response.Equipment.Add(equipments);
                        }
                        //foreach (var eventDatas in equipments.EventData)
                        //{
                        //    //Assign value is not declared in SRS document.
                        //    //eventAttrib.EventID=
                        //    //eventAttrib.EventDateTime=
                        //    //eventAttrib.EventParamName=
                        //    //eventAttrib.EventParamValue=
                        //    eventDatas.EventAttrib.Add(eventAttrib);
                        //}
                        response.ResponseCode = responseCode;
                        response.ResponseText = responseText;
                    }
                }
            }
            foreach (var parameterMasterModel in parameterMasterModelList)
            {
                Equipments equipments = new Equipments();
                IntervalData intervalData = new IntervalData();
                List<IntervalDataModel> intervalDataList = new List<IntervalDataModel>();
                if (parameterMasterModel.ParamType == "Interval")
                {
                    if (meterDataRequest.EstValues == "Y")
                    {
                        intervalDataList = intervalData.GetIntervalDataByParam(parameterMasterModel.ID, meterDataRequest.StarDateTime, meterDataRequest.EndDateTime);
                        if (intervalDataList.Count == 0)
                        {
                            responseCode = "1";
                            responseText = "No Data Available";
                        }

                        if (!String.IsNullOrEmpty(meterDataRequest.EstValues))
                        {
                            if (meterDataRequest.EstValues == "N")
                                intervalDataList = intervalDataList.Where(x => x.Status != "EST").ToList();
                        }

                        List<MeterRead> meterReadList = new List<MeterRead>();
                        List<EventData> eventDataList = new List<EventData>();
                        EventData eventData = new EventData();
                        EventAttrib eventAttrib = new EventAttrib();
                        equipments.MeterRead = meterReadList;
                        equipmentsList.Add(equipments);
                        foreach (var intervalDatas in intervalDataList)
                        {
                            MeterRead meterRead = new MeterRead();
                            response.ObjectIdentifier = meterDataRequest.ObjectIdentifier;
                            response.ObjectId = meterDataRequest.ObjectId;
                            equipmentModel = equipmentObj.GetEquipmentById(meterID);
                            equipments.EquipmentID = equipmentModel.MeterID;
                            equipments.MeterReadCount = intervalDataList.Count;
                            meterRead.ParamName = paramName;
                            meterRead.ParamValue = intervalDatas.ParamValue;
                            meterRead.DateTime = intervalDatas.IntervalEndTime;
                            meterRead.Status = intervalDatas.Status;
                            meterRead.Flag = intervalDatas.Flag;
                            meterRead.DataSource = intervalDatas.DataSource.ToString();
                            meterReadList.Add(meterRead);

                            //Assign value is not declared in SRS document.
                            //eventData.EventHeader.EventID= 
                            //eventData.EventHeader.EventDesc= 
                            //eventData.EventHeader.EventDateTime= 
                            //eventData.EventHeader.ReportingDateTime= 
                            //eventData.EventHeader.Source=
                            eventDataList.Add(eventData);
                            //response.Equipment.Add(equipments);
                        }
                        //foreach (var eventDatas in equipments.EventData)
                        //{
                        //    //Assign value is not declared in SRS document.
                        //    //eventAttrib.EventID=
                        //    //eventAttrib.EventDateTime=
                        //    //eventAttrib.EventParamName=
                        //    //eventAttrib.EventParamValue=
                        //    eventDatas.EventAttrib.Add(eventAttrib);
                        //}
                        response.ResponseCode = responseCode;
                        response.ResponseText = responseText;
                    }
                }
            }
            return response;
        }
    }
}
