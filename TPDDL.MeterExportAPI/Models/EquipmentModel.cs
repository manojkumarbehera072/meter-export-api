﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPDDL.MeterExportAPI.Models
{
    public class EquipmentModel
    {
        public int ID { get; set; }
        public string MeterID { get; set; }
        public string UtilityID { get; set; }
        public string AltUtilityID { get; set; }
    }
}