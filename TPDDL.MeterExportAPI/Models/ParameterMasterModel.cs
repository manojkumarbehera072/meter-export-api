﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPDDL.MeterExportAPI.Models
{
    public class ParameterMasterModel
    {
        public int ID { get; set; }
        public string ParamType { get; set; }
    }
}